/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React, { Component } from 'react';

 import {
   Image,
   SafeAreaView,
   ScrollView,
   StatusBar,
   StyleSheet,
   Text,
   View,  
   TextInput,
 } from 'react-native';
 
 // import Carousel from 'react-native-snap-carousel';
 
 export default class App extends Component {
 //   state = {
 //     activeIndex:0,
 //     carouselItems: [
 //       {
 //           title:"Item 1",
 //           text: "Text 1",
 //       },
 //       {
 //           title:"Item 2",
 //           text: "Text 2",
 //       },
 //       {
 //           title:"Item 3",
 //           text: "Text 3",
 //       },
 //       {
 //           title:"Item 4",
 //           text: "Text 4",
 //       },
 //       {
 //           title:"Item 5",
 //           text: "Text 5",
 //       },
 //     ]
 //   }
 
 //   _renderItem({item,index}){
 //     return (
 //       <View style={{
 //           backgroundColor:'floralwhite',
 //           borderRadius: 5,
 //           height: 250,
 //           padding: 50,
 //           marginLeft: 25,
 //           marginRight: 25, }}>
 //         <Text style={{fontSize: 30}}>{item.title}</Text>
 //         <Text>{item.text}</Text>
 //       </View>
 
 //     )
 // }
   render() {
     return (
   <SafeAreaView  >
     <StatusBar barStyle='light-content' />
     <ScrollView style={styles.scrollView}>
     
     <View style={{backgroundColor:'#27ae60',headerMode: 'screen',height:'100%',}}>
       <View style={{marginTop:15,marginLeft:15,marginRight:15}}>
         <View style={{ flexDirection: "row"}} >      
           <View style={{ flex: 1}}>
             <View style={styles.header_text}>
               <Text style={styles.header_text}>Hey,</Text>
               <Text style={styles.header_text}>Lets search your fish.</Text>
             </View>
           </View>
           <View >
             <View></View>
             <Image style={styles.tinyLogo} source={require('./imgs/logo.png')} />
           </View>
         </View>
         <View>
         <TextInput
         style={styles.search_input}
           placeholder="Search fishes..."
         />
         </View>
         
       </View>
       <View style={styles.main_content}>
         
         <View style={styles.cat_content}>
           <View style={{ flexDirection: "row",borderBottomWidth:1,borderBottomColor:'#d5d5d5',paddingBottom:15,marginBottom:15}}>
             <View style={{ flex: 1}}>
               <View>
                 <Text style={styles.cat_header_left}>Categories</Text>
               </View>
             </View>
             <View >
               <Text style={styles.cat_header_right}>View All</Text>
             </View>
           </View>
           <View style={{ flexDirection: "row",}}>
             <View style={{ flex:1,margin:5, }}>
               <View style={styles.cat_box}>
                 <View style={styles.cat_box_bg}>
                   <Image style={styles.cat_image} source={require('./imgs/breaked-selmon.png')} />
                 </View>
                 <View style={styles.cat_title_content}>
                   <Text style={styles.cat_title}>Beaked Salmon</Text>
                 </View>
                                 
               </View>
             </View>
             <View style={{ flex:1,margin:5 }}>
               <View style={styles.cat_box}>
                 <View style={styles.cat_box_bg}>
                   <Image style={styles.cat_image} source={require('./imgs/black-pomfret.png')} />
                 </View>
                 <View>
                   <Text style={styles.cat_title}>Black Pomfret</Text>
                 </View>               
               </View>
             </View>
             <View style={{ flex:1,margin:5 }}>
               <View style={styles.cat_box}>
                 <View style={styles.cat_box_bg}>
                   <Image style={styles.cat_image} source={require('./imgs/butter-fish.png')} />
                 </View>
                 <View>
                   <Text style={styles.cat_title}>Butter Fish</Text>
                 </View>                
               </View>
             </View>
             
             
           </View>
           <View style={{ flexDirection: "row",}}>
             <View style={{ flex:1,margin:5, }}>
               <View style={styles.cat_box}>
                 <View style={styles.cat_box_bg}>
                   <Image style={styles.cat_image} source={require('./imgs/bombay-duck.png')} />
                 </View>
                 <View style={styles.cat_title_content}>
                   <Text style={styles.cat_title}>Bombay Duck</Text>
                 </View>
                                 
               </View>
             </View>
             <View style={{ flex:1,margin:5 }}>
               <View style={styles.cat_box}>
                 <View style={styles.cat_box_bg}>
                   <Image style={styles.cat_image} source={require('./imgs/img2.png')} />
                 </View>
                 <View>
                   <Text style={styles.cat_title}>Parrot Fish</Text>
                 </View>               
               </View>
             </View>
             <View style={{ flex:1,margin:5 }}>
               <View style={styles.cat_box}>
                 <View style={styles.cat_box_bg}>
                   <Image style={styles.cat_image} source={require('./imgs/img1.png')} />
                 </View>
                 <View>
                   <Text style={styles.cat_title}>Bluefish</Text>
                 </View>                
               </View>
             </View>
             
             
           </View>
           {/* <View style={{ flex: 1, flexDirection:'row', justifyContent: 'center', }}>
                 <Carousel
                   layout={"default"}
                   ref={ref => this.carousel = ref}
                   data={this.state.carouselItems}
                   sliderWidth={300}
                   itemWidth={300}
                   renderItem={this._renderItem}
                   onSnapToItem = { index => this.setState({activeIndex:index}) } />
             </View> */}
         </View>
         <View style={styles.offer_content}>
 
         </View>
         <View style={styles.cat_content}>
           <View style={{ flexDirection: "row",borderBottomWidth:1,borderBottomColor:'#d5d5d5',paddingBottom:15,marginBottom:15}}>
             <View style={{ flex: 1}}>
               <View>
                 <Text style={styles.cat_header_left}>Popular Deals</Text>
               </View>
             </View>
             <View >
               <Text style={styles.cat_header_right}>View All</Text>
             </View>
           </View>
           
           <View style={{ flexDirection: "row",}}>
           <View style={{ flex:1,margin:5, }}>
               <View style={styles.product_box}>
                 <View style={styles.product_box_bg}>
                   <Image style={styles.product_image} source={require('./imgs/blue-marlin.png')} />
                 </View>
                 <View style={styles.product_title_content}>
                   <Text style={styles.product_title}>Blue Marlin</Text>
                   <Text style={styles.product_price}>₹499</Text>
                 </View>                                
               </View>
             </View>
             <View style={{ flex:1,margin:5, }}>
               <View style={styles.product_box}>
                 <View style={styles.product_box_bg}>
                   <Image style={styles.product_image} source={require('./imgs/socky-selmon.png')} />
                 </View>
                 <View style={styles.product_title_content}>
                   <Text style={styles.product_title}>Socky Selmon</Text>
                   <Text style={styles.product_price}>₹399</Text>
                 </View>                                
               </View>
             </View>          
           </View>
           <View style={{ flexDirection: "row",}}>
             <View style={{ flex:1,margin:5, }}>
               <View style={styles.product_box}>
                 <View style={styles.product_box_bg}>
                   <Image style={styles.product_image} source={require('./imgs/skipjack-tuna.png')} />
                 </View>
                 <View style={styles.product_title_content}>
                   <Text style={styles.product_title}>Skipjack Tuna</Text>
                   <Text style={styles.product_price}>₹999</Text>
                 </View>                                
               </View>
             </View>
             <View style={{ flex:1,margin:5, }}>
               <View style={styles.product_box}>
                 <View style={styles.product_box_bg}>
                   <Image style={styles.product_image} source={require('./imgs/gilt-head-bream.png')} />
                 </View>
                 <View style={styles.product_title_content}>
                   <Text style={styles.product_title}>Gilt Head Bream</Text>
                   <Text style={styles.product_price}>₹550</Text>
                 </View>                                
               </View>
             </View>             
           </View>
           
         </View>
       </View>
       
     </View>
     </ScrollView>
   </SafeAreaView>
     );
   }
 }
 
 const styles = StyleSheet.create({
   scrollView: {
     // backgroundColor: 'pink',
   },
   container: {
     flex: 1,
     flexDirection: "column"
   },
   search_input:{
     backgroundColor:'#fff',
     borderRadius:5,
     paddingHorizontal:15,
     marginVertical:15
   },
   header_text: {
     color:'#fff',
     fontSize:20,
   },
   main_content: {
     backgroundColor:'#f9f9f9',
     height:'100%',
     borderTopLeftRadius:30,
     borderTopRightRadius:30,
     padding:10
   },
   cat_content: {
     backgroundColor: '#fff',
     borderRadius:25,
     padding:15,
     // paddingBottom:40
     
   },
   cat_header_left: {
     fontSize:20,
     fontWeight:'bold',
     color: '#000',
   },
   cat_header_right: {
     color:'#3bb56e',
     fontSize:16,
     fontWeight:'bold'
   },
   cat_box: {
     width:100,
   },
   cat_box_bg: {
     backgroundColor: '#e9f7ef',
     borderRadius:10,
     
   },
   cat_image: {
     width: 65,
     height: 65,
     // borderRadius:50,
     margin:20,
     resizeMode:'contain'
   },
   cat_title_content: {
     
   },
   cat_title: {
     textAlign:'center',
     fontSize:16,
     marginVertical:10,
     
     fontWeight:'bold'   
   },
   product_box: {
     
   },
   product_box_bg: {
     backgroundColor: '#e9f7ef',
     borderRadius:10,
     alignItems:'center',
     
   },
   product_image: {
     width: 155,
     height: 100,
     // borderRadius:50,
     margin:20,
     resizeMode:'center',
     
   },
   product_title_content: {
     
   },
   product_title: {
     // textAlign:'center',
     // paddingLeft:15,
     fontSize:16,
     marginTop:10,
     fontWeight:'bold'   
   },
   product_price: {
     color: '#0d0d0d',
   },
   offer_content: {
     backgroundColor: '#bee7cf',
     borderRadius:25,
     height:150,
     marginVertical:15
   },
   sectionContainer: {
     marginTop: 32,
     padding:22,
     paddingHorizontal: 24,
   },
   sectionTitle: {
     padding: 15,
     color: '#f00',
     fontSize: 24,
     fontWeight: '600',
   },
   sectionDescription: {
     marginTop: 8,
     fontSize: 18,
     fontWeight: '400',
   },
   highlight: {
     fontWeight: '700',
   },
   tinyLogo: {
     width: 50,
     height: 50,
     borderRadius:50
   },
   logo: {
     width: 66,
     height: 58,
   },
   image: {
     flex: 1,
     resizeMode: "cover",
     justifyContent: "center"
   },
   text: {
     color: "white",
     fontSize: 42,
     height:40,
     fontWeight: "bold",
     textAlign: "center",
     backgroundColor: "#000000a0"
   },
 });
 